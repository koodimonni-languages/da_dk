��    %      D      l      l  
   m  	   x  G   �     �     �     �     �  E      W   F     �     �     �  
   �  
   �     �     �     �     �               #     0  F   F     �     �  \   �       &      0   G  $   x     �  #   �     �  =   �  *   ;     f  �   y     q     �  H   �     �     �            Z     Z   y     �     �     �     	     	     $	     2	     F	     N	     W	     h	     z	     �	  F   �	     �	     �	  V   

     a
     q
     }
  
   �
     �
  
   �
     �
     �
  +   �
     �
   % Comments 1 Comment Applied to the header on small screens and the sidebar on wide screens. Comment navigation Comments are closed. Edit Featured It looks like nothing was found at this location. Maybe try a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment Newer Comments Next Next Image Next post: Nothing Found Older Comments Pages: Previous Previous Image Previous post: Primary Menu Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Search Results for: %s Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Twenty Fifteen Used before category names.Categories Used before full size attachment link.Full size Used before post author name.Author Used before post format.Format Used before publish date.Posted on Used before tag names.Tags Used between list items, there is a space after the comma.,  https://wordpress.org/themes/twentyfifteen the WordPress team PO-Revision-Date: 2014-12-15 10:31:50+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.0-alpha-1000
Project-Id-Version: Twenty Fifteen
 % kommentarer  1 kommentar  Bruges til sidehovedet på små skærme og sidebaren på brede skærme.  Kommentar navigation  Der er lukket for kommentarer.  Rediger  Udvalgt  Det ser ud til, at vi ikke kan finde det, du leder efter. Måske vil en søgning hjælpe.  Det ser ud til, at vi ikke kan finde det, du leder efter. Måske vil en søgning hjælpe.  Skriv en kommentar  Nyere kommentarer  Næste  Næste billede  Næste indlæg:  Intet fundet  Ældre kommentarer  Sider:  Forrige  Forrige billede  Forrige indlæg:  Primær menu  Drevet af %s  Klar til at udgive dit første indlæg? <a href="%1$s">Start her</a>.  Søgeresultater for: %s  Videre til indhold  Beklager, men din søgning gav ingen resultater. Prøv igen med nogle andre søgeord.  Twenty Fifteen  Kategorier  Original størrelse  Forfatter  Format  Udgivet i  Tags  ,   https://wordpress.org/themes/twentyfifteen  holdet bag WordPress  